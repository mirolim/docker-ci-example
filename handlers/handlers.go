package handler

import (
	"fmt"
	"net/http"

	"docker-ci-example/person"

	"github.com/zenazn/goji/web"
)

// http handler to set person age
func IsNameValid(c web.C, w http.ResponseWriter, r *http.Request) {
	// default status OK
	status := "OK"
	var p person.Self
	// set status
	name := c.URLParams["name"]
	err := p.SetName(name)
	// if error change status to BAD
	if err != nil {
		status = "BAD"
	}
	// return status
	fmt.Fprintf(w, "%s", status)
}
