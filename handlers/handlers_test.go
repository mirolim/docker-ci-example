package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/zenazn/goji/web"
)

// http handler to set person age
func TestHandlerIsNameValid(t *testing.T) {
	testNames := map[string]string{
		"":     "BAD",
		"Mike": "OK",
		"VeryLongDescriptiveNameWhichWeConsiderInvalid": "BAD",
	}
	for k, v := range testNames {
		req, err := http.NewRequest("GET", "http://some.domain/checkname/"+k, nil)
		if err != nil {
			t.Errorf("http NewRequest Err %v", err)
		}
		// record response from handler
		w := httptest.NewRecorder()
		// create goji context
		var c web.C
		// init map
		c.URLParams = make(map[string]string)
		// set values passed
		c.URLParams["name"] = k
		// feed request to tested handler
		IsNameValid(c, w, req)
		// check result
		if w.Body.String() != v {
			t.Errorf("got %s, want  %s with arg %s", w.Body.String(), v, k)
		}
	}
}
