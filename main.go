package main

import (
	"docker-ci-example/handlers"

	"github.com/zenazn/goji"
)

func main() {
	goji.Get("/checkname/:name", handler.IsNameValid)
	goji.Serve()
}
