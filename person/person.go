package person

import "errors"

type Self struct {
	age  int
	name string
}

const (
	MinAge = 0
	MaxAge = 160

	MinNameLen = 1
	MaxNameLen = 20
)

var (
	InvalidAge  = errors.New("Invalid Age")
	InvalidName = errors.New("Invalid Name")
)

// set person age if valid
func (p *Self) SetAge(i int) error {
	if i < MinAge || i > MaxAge {
		return InvalidAge
	}
	// set age
	p.age = i
	return nil
}

// set person name if valid
func (p *Self) SetName(s string) error {
	if len(s) < MinNameLen || len(s) > MaxNameLen {
		return InvalidName
	}
	// set name
	p.name = s
	return nil
}

// get person name
func (p Self) Age() int {
	return p.age
}

// get person name
func (p Self) Name() string {
	return p.name
}
